

import { SkCheckboxImpl }  from '../../sk-checkbox/src/impl/sk-checkbox-impl.js';


export class DefaultSkCheckbox extends SkCheckboxImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'checkbox';
    }

    get input() {
        return this.comp.el.querySelector('input');
    }

    get checked() {
        return this.input.hasAttribute('checked');
    }

    set checked(checked) {
        if (checked) {
            this.input.checked = 'checked';
            this.input.setAttribute('checked', 'checked');
        } else {
            this.input.checked = false;
            this.input.removeAttribute('checked');
        }
    }
    // sk-checkbox element's context
    onClick(event) {
        this.checked = ! this.checked;
        if (this.checked) {
            this.inact = true;
            this.comp.setAttribute('checked', 'checked');
            this.inact = false;
        } else {
            this.inact = true;
            this.comp.removeAttribute('checked');
            this.inact = false;
        }
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.comp.inputEl.setCustomValidity(result);
            this.showInvalid();
        }
    }

    bindEvents() {
        if (this.checked) {
            this.input.setAttribute('checked', this.checked);
        }
        this.comp.onclick = function(event) {
            this.onClick(event);
        }.bind(this);
    }


    enable() {
        super.enable();
    }

    unbindEvents() {
        super.unbindEvents();
        this.comp.removeEventListener('click', this.clickHandle);
    }

    showInvalid() {
        super.showInvalid();
    }

    showValid() {
        super.showValid();
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


    restoreState(state) {
        super.restoreState(state);
        let disabled = this.comp.getAttribute('disabled');
        if (disabled) {
            this.input.setAttribute('disabled', 'disabled');
        }
    }
}
